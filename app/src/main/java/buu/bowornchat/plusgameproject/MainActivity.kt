package buu.bowornchat.plusgameproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val arrayAdapter : ArrayAdapter<*>
        val games = arrayOf("+ Plus","- Minus","x Multiply")
        val ListGame = findViewById<ListView>(R.id.ListGame)
        arrayAdapter = ArrayAdapter(this,android.R.layout.simple_list_item_1,games)
        ListGame.adapter = arrayAdapter
        ListGame.setOnItemClickListener { parent,view,position,id->
            val intent = Intent(this@MainActivity,MathViewActivity::class.java)
            intent.putExtra("Math",games[position].substring(0,1))
            startActivity(intent)
        }
    }
}