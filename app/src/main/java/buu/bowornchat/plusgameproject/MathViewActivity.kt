package buu.bowornchat.plusgameproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import java.lang.Integer.parseInt

class MathViewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_math_view)

        val mathString: String = intent.getStringExtra("Math") ?: "unknown"
        val txtMath = findViewById<TextView>(R.id.txtMath)
        txtMath.text = mathString

        val txtnumber1 = findViewById<TextView>(R.id.txtNum1)
        val txtnumber2 = findViewById<TextView>(R.id.txtNum2)
        val btnNumber1 = findViewById<Button>(R.id.btnNumber1)
        val btnNumber2 = findViewById<Button>(R.id.btnNumber2)
        val btnNumber3 = findViewById<Button>(R.id.btnNumber3)
        val txtResult = findViewById<TextView>(R.id.txtResult)
        val txtWin = findViewById<TextView>(R.id.txtWin)
        val txtLose = findViewById<TextView>(R.id.txtLose)
        round(txtnumber1, txtnumber2, btnNumber1, btnNumber2, btnNumber3, txtMath)

        btnNumber1.setOnClickListener {
            if (checkTrue(txtnumber1, txtnumber2, btnNumber1,txtMath)) {
                txtResult.text = "ถูกต้อง"
                Win(txtWin)
            } else {
                txtResult.text = "ไม่ถูกต้อง"
                Lose(txtLose)
            }
            round(txtnumber1, txtnumber2, btnNumber1, btnNumber2, btnNumber3, txtMath)
        }
        btnNumber2.setOnClickListener {
            if (checkTrue(txtnumber1, txtnumber2, btnNumber2,txtMath)) {
                txtResult.text = "ถูกต้อง"
                Win(txtWin)
            } else {
                txtResult.text = "ไม่ถูกต้อง"
                Lose(txtLose)
            }
            round(txtnumber1, txtnumber2, btnNumber1, btnNumber2, btnNumber3, txtMath)
        }
        btnNumber3.setOnClickListener {
            if (checkTrue(txtnumber1, txtnumber2, btnNumber3,txtMath)) {
                txtResult.text = "ถูกต้อง"
                Win(txtWin)
            } else {
                txtResult.text = "ไม่ถูกต้อง"
                Lose(txtLose)
            }
            round(txtnumber1, txtnumber2, btnNumber1, btnNumber2, btnNumber3, txtMath)
        }
    }

    fun round(
        txtnumber1: TextView,
        txtnumber2: TextView,
        btnNumber1: Button,
        btnNumber2: Button,
        btnNumber3: Button,
        txtMath: TextView
    ) {
        txtnumber1.text = randomNumber().toString()
        txtnumber2.text = randomNumber().toString()
        val result = checkMathString(txtMath, txtnumber1, txtnumber2)
        when (randomButton()) {
            0 -> {
                btnNumber1.text = result.toString()
                btnNumber2.text = (result + 1).toString()
                btnNumber3.text = (result + 2).toString()
            }
            1 -> {
                btnNumber1.text = (result - 1).toString()
                btnNumber2.text = result.toString()
                btnNumber3.text = (result + 1).toString()
            }
            2 -> {
                btnNumber1.text = (result - 2).toString()
                btnNumber2.text = (result - 1).toString()
                btnNumber3.text = result.toString()
            }
        }
    }

    fun checkTrue(txtnumber1: TextView, txtnumber2: TextView, btnNumber: Button, txtMath: TextView): Boolean {
        val result = checkMathString(txtMath,txtnumber1,txtnumber2)
        if (result == parseInt(btnNumber.text.toString())) {
            return true
        }
        return false
    }

    fun randomNumber(): Int {
        return (0..10).random()
    }

    fun randomButton(): Int {
        return (0..2).random()
    }

    fun Win(txtWin: TextView) {
        txtWin.text = (parseInt(txtWin.text.toString()) + 1).toString()
    }

    fun Lose(txtLose: TextView) {
        txtLose.text = (parseInt(txtLose.text.toString()) + 1).toString()
    }

    fun checkMathString(txtMath: TextView, txtnumber1: TextView, txtnumber2: TextView): Int {
        when (txtMath.text) {
            "+" -> return parseInt(txtnumber1.text.toString()) + parseInt(txtnumber2.text.toString())
            "-" -> return parseInt(txtnumber1.text.toString()) - parseInt(txtnumber2.text.toString())
            "x" -> return parseInt(txtnumber1.text.toString()) * parseInt(txtnumber2.text.toString())
        }
        return 0
    }
}